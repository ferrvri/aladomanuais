import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HttpService } from 'src/app/_services/HttpService/http.service';

@Component({
  selector: 'app-termos-modal',
  templateUrl: './termos-modal.page.html',
  styleUrls: ['./termos-modal.page.scss'],
})
export class TermosModalPage implements OnInit {

  termos = ''

  constructor(
    public _modalController: ModalController,
    private _http: HttpService
  ) { }

  ngOnInit() {
    this._http.get(
      'termos'
    ).then( (response: any) => {
      console.log(response)
      if (response.status && response.status == true){
        this.termos = response.result[0].condicoes.split('\r\n')
      }
    })
  }

}
