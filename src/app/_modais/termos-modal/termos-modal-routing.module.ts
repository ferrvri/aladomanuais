import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TermosModalPage } from './termos-modal.page';

const routes: Routes = [
  {
    path: '',
    component: TermosModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TermosModalPageRoutingModule {}
