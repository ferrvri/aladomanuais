import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TermosModalPageRoutingModule } from './termos-modal-routing.module';

import { TermosModalPage } from './termos-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TermosModalPageRoutingModule
  ],
  declarations: [TermosModalPage]
})
export class TermosModalPageModule {}
