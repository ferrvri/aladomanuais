import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TermosModalPage } from './termos-modal.page';

describe('TermosModalPage', () => {
  let component: TermosModalPage;
  let fixture: ComponentFixture<TermosModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermosModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TermosModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
