import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { HttpService } from 'src/app/_services/HttpService/http.service';

@Component({
  selector: 'app-pesquisa-satisfacao',
  templateUrl: './pesquisa-satisfacao.page.html',
  styleUrls: ['./pesquisa-satisfacao.page.scss'],
})
export class PesquisaSatisfacaoPage implements OnInit {

  alreadyDid = false;

  sugestao = '';

  questoes = [
    {
      titulo: 'Grau de satisfação com os produtos e serviços oferecidos pelo Alado?',
      grau: [
        {
          titulo: 'Muito satisfeito',
          value: false
        },
        {
          titulo: 'Satisfeito',
          value: false
        },
        {
          titulo: 'Insatisfeito',
          value: false
        },
        {
          titulo: 'Indiferente',
          value: false
        },
        {
          titulo: 'Ruim',
          value: false
        }
      ]
    },
    {
      titulo: 'Qual seu grau de satisfação ao encontrar produtos nas lojas?',
      grau: [
        {
          titulo: 'Muito satisfeito',
          value: false
        },
        {
          titulo: 'Satisfeito',
          value: false
        },
        {
          titulo: 'Insatisfeito',
          value: false
        },
        {
          titulo: 'Indiferente',
          value: false
        },
        {
          titulo: 'Ruim',
          value: false
        }
      ]
    },
    {
      titulo: 'Qual o seu grau de satisfação em ralação as garantias dos produtos?',
      grau: [
        {
          titulo: 'Muito satisfeito',
          value: false
        },
        {
          titulo: 'Satisfeito',
          value: false
        },
        {
          titulo: 'Insatisfeito',
          value: false
        },
        {
          titulo: 'Indiferente',
          value: false
        },
        {
          titulo: 'Ruim',
          value: false
        }
      ]
    }
  ]

  constructor(
    public _modalController: ModalController,
    private _alert: AlertController,
    private _http: HttpService
  ) { }

  ngOnInit() {
    if (localStorage.getItem('pesquisa') && localStorage.getItem('pesquisa') == 'true'){
      this.alreadyDid = true
    }else{
      this.alreadyDid = false
    }
  }

  select(pergunta, grau) {
    pergunta.grau.forEach(p => {
      if (grau.titulo != p.titulo) {
        p.value = false
      }
    })
  }

  async enviar() {
    let f = this.questoes.filter(q => {
      return q.grau.filter(g => { return g.value == false }).length == q.grau.length
    })

    if (f.length > 0) {
      let _alert = await this._alert.create({
        animated: true,
        backdropDismiss: true,
        header: 'Campos vazios!',
        subHeader: 'Preencha toda a pesquisa para continuar',
        buttons: [
          { text: 'OK' }
        ]
      })

      _alert.present();
    } else {
      this._http.post(
        'pesquisa',
        {
          q1: this.questoes[0].grau.filter(g => {return g.value == true})[0].titulo,
          q2: this.questoes[1].grau.filter(g => {return g.value == true})[0].titulo,
          q3: this.questoes[2].grau.filter(g => {return g.value == true})[0].titulo,
          sugestao: this.sugestao
        }
      ).then( (response : any) => {
        if (response.status && response.status == true){
          this.alreadyDid = true
          localStorage.setItem('pesquisa', 'true');
        }
      })
    }
  }
}
