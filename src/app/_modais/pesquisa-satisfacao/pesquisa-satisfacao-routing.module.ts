import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PesquisaSatisfacaoPage } from './pesquisa-satisfacao.page';

const routes: Routes = [
  {
    path: '',
    component: PesquisaSatisfacaoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PesquisaSatisfacaoPageRoutingModule {}
