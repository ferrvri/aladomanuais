import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PesquisaSatisfacaoPage } from './pesquisa-satisfacao.page';

describe('PesquisaSatisfacaoPage', () => {
  let component: PesquisaSatisfacaoPage;
  let fixture: ComponentFixture<PesquisaSatisfacaoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisaSatisfacaoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PesquisaSatisfacaoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
