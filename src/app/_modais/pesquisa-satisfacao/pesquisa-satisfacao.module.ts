import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PesquisaSatisfacaoPageRoutingModule } from './pesquisa-satisfacao-routing.module';

import { PesquisaSatisfacaoPage } from './pesquisa-satisfacao.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PesquisaSatisfacaoPageRoutingModule
  ],
  declarations: [PesquisaSatisfacaoPage],
  exports: []
})
export class PesquisaSatisfacaoPageModule {}
