import { Component } from '@angular/core';
//import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ModalController } from '@ionic/angular';
import { PesquisaSatisfacaoPage } from '../_modais/pesquisa-satisfacao/pesquisa-satisfacao.page';
import { TermosModalPage } from '../_modais/termos-modal/termos-modal.page';
import { SobrePage } from '../_modais/sobre/sobre.page';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(
    //private _iab: InAppBrowser,
    private _modalController: ModalController
  ) {}

  openPainel(){
    //const browser = this._iab.create('https://alado.com.br/pedidos/');
    //browser.show()
  }

  closeMais(){
    document.getElementById('mais_div').classList.add('slide_left')
    document.getElementById('mais_div').classList.remove('slide_right')
    setTimeout(_ => {
      document.getElementById('mais').style.display = 'none'
    }, 750)
  }

  openMais(){
    document.getElementById('mais_div').classList.add('slide_right')
    document.getElementById('mais_div').classList.remove('slide_left')
    document.getElementById('mais').style.display = 'block'
  }

  async sobre(){
    let modal = await this._modalController.create({
      component: SobrePage,
      animated: true,
      mode: 'ios',
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true
    })

    modal.present()
  }

  async pesquisa(){
    let modal = await this._modalController.create({
      component: PesquisaSatisfacaoPage,
      animated: true,
      mode: 'ios',
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true
    })

    modal.present()
  }

  async termos(){
    let modal = await this._modalController.create({
      component: TermosModalPage,
      animated: true,
      mode: 'ios',
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true
    })

    modal.present()
  }
}
