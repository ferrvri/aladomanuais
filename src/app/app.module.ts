import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';

import { File } from '@ionic-native/file/ngx';
import { StorageService } from './_services/Storage/storage.service';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { HTTP } from '@ionic-native/http/ngx';
//import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
import { PesquisaSatisfacaoPage } from './_modais/pesquisa-satisfacao/pesquisa-satisfacao.page';
import { PesquisaSatisfacaoPageModule } from './_modais/pesquisa-satisfacao/pesquisa-satisfacao.module';
import { TermosModalPageModule } from './_modais/termos-modal/termos-modal.module';
import { SobrePageModule } from './_modais/sobre/sobre.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,

    PesquisaSatisfacaoPageModule,
    TermosModalPageModule,
    SobrePageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    File,
    StorageService,
    FileOpener,
    HTTP,
    //InAppBrowser,
    NativePageTransitions
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
