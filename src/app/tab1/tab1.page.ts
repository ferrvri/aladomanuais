import { Component, OnInit, Sanitizer, ViewChild } from '@angular/core';
import { HttpService } from '../_services/HttpService/http.service';

import { File } from '@ionic-native/file/ngx';
import { StorageService } from '../_services/Storage/storage.service';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { AlertController, IonSlides, Platform } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  constructor(
    private _http: HttpService,
    private _storage: StorageService,
    private _file: File,
    private _fileOpener: FileOpener,
    private _alert: AlertController,
    private _sanitizer: DomSanitizer,
    private _nativePageTransitions: NativePageTransitions,
    public _platform: Platform
  ) { }

  @ViewChild(IonSlides, { static: false }) slides: IonSlides;

  loading = false
  getItemsError = false

  items = []
  categorias = []

  PAGE = 1

  searchKeyword = ''
  categoriaItemSize = 0
  categoriaIndex = 0
  categoriaSelected = {
    items: []
  };

  ionViewWillLeave() {
    let options: NativeTransitionOptions = {
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 50,
      androiddelay: 75,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }

    this._nativePageTransitions.flip(options);
  }

  ngOnInit() {
    this._platform.ready().then(_ => {
      this._storage.init()
      this.loading = true
      this.PAGE = 1
  
      this._http.get(
        'categorias'
      ).then((response: any) => {
        if (response.status && response.status == true) {
          this.loading = false;
  
          this.categorias = response.result
          this._storage.saveContent(this.categorias, 'categorias')
  
          this.categorias.forEach(categoria => {
            this._http.get(
              'items/categoria/' + categoria.cat_id
            ).then((response2: any) => {
              if (response2.status && response2.status == true) {
                this.getItemsError = false
                response2.result.forEach(r => {
                  r.imageURL = this._sanitizer.bypassSecurityTrustUrl(this._http.httpURL + 'imagem/' + r.item_image_url)
                  r.downloading = false
                  let pdfs = this._storage.getContent('pdfs')
  
                  let p = pdfs.filter(p => {
                    return p.item.item_id == r.item_id
                  })[0]
  
                  if (p !== undefined) {
                    r.pdfURL = p.url
                  }
                })
                this.items = response2.result
  
                categoria.items = this.items
              }
  
  
            }).catch(async e => {
              this.loading = false;
              (await this._alert.create({
                animated: true,
                backdropDismiss: true,
                buttons: [
                  {
                    text: 'OK'
                  }
                ],
                message: 'Erro no servidor!',
                subHeader: JSON.stringify(e)
              })).present();
            })
          })
  
          this.categoriaSelected = this.categorias[0]
          this.categoriaItemSize = Math.floor(100 / this.categorias.length)
          this.loading = false
        } else {
          this.loading = false
          this.getItemsError = true
        }
  
        this.loading = false
      })
    })
  }

  doRefresh(event) {
    this.PAGE = 1
    this.categoriaIndex = 0
    this.categoriaSelected = this.categorias[this.categoriaIndex]
    this.categoriaItemSize = Math.floor(100 / this.categorias.length);

    this.loading = true
    this.items = []

    this._http.get(
      'categorias'
    ).then((response: any) => {
      if (response.status && response.status == true) {
        this.categorias = response.result
        this._storage.saveContent(this.categorias, 'categorias')

        this.categorias.forEach(categoria => {
          this._http.get(
            'items/categoria/' + categoria.cat_id
          ).then((response2: any) => {
            if (response2.status && response2.status == true) {
              this.getItemsError = false
              response2.result.forEach(r => {
                r.imageURL = this._sanitizer.bypassSecurityTrustUrl(this._http.httpURL + 'imagem/' + r.item_image_url)
                r.downloading = false
                let pdfs = this._storage.getContent('pdfs')

                let p = pdfs.filter(p => {
                  return p.item.item_id == r.item_id
                })[0]

                if (p !== undefined) {
                  r.pdfURL = p.url
                }
              })
              this.items = response2.result

              categoria.items = this.items
            }

            this.loading = false
          }).catch(async e => {
            this.loading = false;
            (await this._alert.create({
              animated: true,
              backdropDismiss: true,
              buttons: [
                {
                  text: 'OK'
                }
              ],
              message: 'Erro no servidor!',
              subHeader: JSON.stringify(e)
            })).present();
          })
        })

        console.log(this.categorias)

        this.categoriaItemSize = Math.floor(100 / this.categorias.length)
        this.categoriaSelected = this.categorias[0]
        this.loading = false
      }

      setTimeout(_ => event.target.complete(), 1000);
    })
  }

  downloadPDF(item, event) {
    event.preventDefault()

    item.downloading = true

    this._http._get(
      this._http.httpURL + 'pdf/' + item.item_pdf_url
    ).subscribe((response) => {
      let filedir = this._file.cacheDirectory;
      this._file.writeFile(filedir, item.item_id + '.pdf', response, { append: false, replace: true }
      ).then(async (arquivo) => {
        item.downloading = false;

        let pdfs = this._storage.getContent('pdfs')

        let p = pdfs.filter(p => {
          return p.item.item_id == item.item_id
        })

        if (p.length < 1) {
          pdfs.push({
            item: item,
            url: filedir + item.item_id + '.pdf'
          })

          this._storage.saveContent(pdfs, 'pdfs')

          let _alert = await this._alert.create({
            animated: true,
            backdropDismiss: false,
            message: 'PDF baixado, deseja abri-lo?',
            buttons: [
              {
                text: 'Não',
                handler: () => {
                  _alert.dismiss()
                }
              },
              {
                text: "Sim",
                handler: () => {
                  this._fileOpener.open(
                    filedir + item.item_id + '.pdf',
                    'application/pdf'
                  )
                    .then(() => { item.downloading = false; })
                    .catch(e => { alert('Open error' + e); item.downloading = false; });
                }
              }
            ]
          })

          _alert.present();
        } else {
          this._fileOpener.open(
            p[0].url,
            'application/pdf'
          )
            .then(() => { item.downloading = false; })
            .catch(e => { alert('Open error' + e), item.downloading = false; });
        }
      }).catch(e => { alert('Save error' + JSON.stringify(e)), item.downloading = false });
    }, async e => {
      this.loading = false;
      (await this._alert.create({
        animated: true,
        backdropDismiss: true,
        buttons: [
          {
            text: 'OK'
          }
        ],
        message: 'Erro no servidor!',
        subHeader: JSON.stringify(e)
      })).present();
    })
  }

  loadData(event) {
    this.PAGE++

    this._http.get(
      'items/categoria/' + this.categorias[this.categoriaIndex].cat_id + '/' + this.PAGE
    ).then((response: any) => {
      if (response.status && response.status == true) {
        this.getItemsError = false
        response.result.forEach(r => {
          r.imageURL = this._sanitizer.bypassSecurityTrustUrl(this._http.httpURL + 'imagem/' + r.item_image_url)

          let pdfs = this._storage.getContent('pdfs')

          let p = pdfs.filter(p => {
            return p.item.item_id == r.item_id
          })[0]

          if (p !== undefined) {
            r.pdfURL = p.url
          }

          r.downloading = false
        })
        this.categorias[this.categoriaIndex].items.push(...response.result)
      } else {
        this.PAGE = 1
      }
    }).catch(async e => {
      this.loading = false;
      (await this._alert.create({
        animated: true,
        backdropDismiss: true,
        buttons: [
          {
            text: 'OK'
          }
        ],
        message: 'Erro no servidor!',
        subHeader: JSON.stringify(e)
      })).present();
    })

    setTimeout(() => {
      event.target.complete();
    }, 500);
  }

  filter() {
    if (this.categoriaSelected.items.length < 1) {
      this.categoriaSelected = this.categorias[0]
    }

    this.categoriaSelected.items = this.categoriaSelected.items.filter(i => {
      return i.item_nome.toLowerCase().indexOf(this.searchKeyword.toLowerCase()) != -1 || i.item_codigo.toLowerCase().indexOf(this.searchKeyword.toLowerCase()) != -1
    })
  }
}
