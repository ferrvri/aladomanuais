import { Component, OnInit } from '@angular/core';
import { HttpService } from '../_services/HttpService/http.service';
import { StorageService } from '../_services/Storage/storage.service';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { AlertController, Platform } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  constructor(
    private _http: HttpService,
    private _storage: StorageService,
    private _file: File,
    private _fileOpener: FileOpener,
    private _alert: AlertController,
    private _sanitizer: DomSanitizer,
    private _nativePageTransitions: NativePageTransitions,
    public _platform: Platform
  ) { }

  loading = false
  getItemsError = false

  items = []
  categorias = []

  searchKeyword = ''

  PAGE = 1

  categoriaItemSize = 0
  categoriaIndex = 0
  categoriaSelected = {
    items: []
  };


  ionViewWillLeave() {
    let options: NativeTransitionOptions = {
      duration: 150,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 50,
      androiddelay: 75,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    }

    this._nativePageTransitions.flip(options);
  }

  ngOnInit() {
    this.loading = true

    this.categorias = this._storage.getContent('categorias')
    let response = this._storage.getContent('pdfs')
    response.forEach(item => {
      item.item.downloading = false;
      let pdfs = this._storage.getContent('pdfs')

      let p = pdfs.filter(p => {
        return p.item.item_id == item.item.item_id
      })[0]

      if (p !== undefined) {
        item.item.pdfURL = p.url
      }

      item.item.imageURL = this._sanitizer.bypassSecurityTrustUrl(this._http.httpURL + 'imagem/' + item.item.item_image_url)

      this.items.push(item.item)
    })

    this.categorias.forEach(categoria => {
      categoria.items = this.items.filter(i => { return i.fk_categoria_id == categoria.cat_id })
    })

    this.loading = false
    
    if (this.categorias.length > 0){
      this.categorias[0].items = this.items.filter(i => { return i.fk_categoria_id == this.categorias[0].cat_id })
      this.categoriaSelected = this.categorias[0]
      this.categoriaItemSize = Math.floor(100 / this.categorias.length);
    }
  }

  doRefresh(event) {
    setTimeout(() => {
      this.loading = true
      this.items = []

      let response = this._storage.getContent('pdfs')
      response.forEach(item => {
        item.item.downloading = false;
        let pdfs = this._storage.getContent('pdfs')

        let p = pdfs.filter(p => {
          return p.item.item_id == item.item.item_id
        })[0]

        if (p !== undefined) {
          item.item.pdfURL = p.url
        }

        item.item.imageURL = this._sanitizer.bypassSecurityTrustUrl(this._http.httpURL + 'imagem/' + item.item.item_image_url)

        this.items.push(item.item)
      })


      this.categorias.forEach(categoria => {
        categoria.items = this.items.filter(i => { return i.fk_categoria_id == categoria.cat_id })
      })

      this.loading = false
      this.categorias[0].items = this.items.filter(i => { return i.fk_categoria_id == this.categorias[0].cat_id })
      this.categoriaSelected = this.categorias[0]
      this.categoriaIndex = 0
      this.categoriaItemSize = Math.floor(100 / this.categorias.length);
      event.target.complete();
    }, 1000);
  }

  downloadPDF(item, event) {
    event.preventDefault();
    item.downloading = true

    let pdfs = this._storage.getContent('pdfs')
    let p = pdfs.filter(p => {
      return p.item.item_id == item.item_id
    })

    this._fileOpener.open(
      p[0].url,
      'application/pdf'
    )
      .then(() => { item.downloading = false; })
      .catch(e => { alert('Open error' + e), item.downloading = false; });
  }

  filter() {
    this.items = this.items.filter(i => {
      return i.item_nome.toLowerCase().indexOf(this.searchKeyword.toLowerCase()) != -1 || i.item_codigo.toLowerCase().indexOf(this.searchKeyword.toLowerCase()) != -1
    })
  }

  loadData(event) {
    if (this.items.length == 15) {
      setTimeout(() => {
        this.PAGE++

        let response = this._storage.getContent('pdfs')
        this.items.push(...response.slice((this.PAGE - 1) * 15, this.PAGE * 15))

        event.target.complete();
      }, 500);
    } else {
      event.target.complete();
    }
  }

  deleteItem(item) {
    console.log(item)

  }
}
