import { Injectable, OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService implements OnInit {

  pdfs = []
  categorias = []

  constructor() { }

  ngOnInit() {
    this.init()
  }

  init() {
    if (localStorage.getItem('pdfs') && localStorage.getItem('categorias')) {
      this.pdfs = JSON.parse(localStorage.getItem('pdfs'))
      this.categorias = JSON.parse(localStorage.getItem('categorias'))
    } else {
      this.pdfs = []
      this.categorias = []
      localStorage.setItem('pdfs', JSON.stringify(this.pdfs))
      localStorage.setItem('categorias', JSON.stringify(this.categorias))
    }
  }

  saveContent(obj, type) {
    switch(type){
      case 'pdfs':{
        localStorage.setItem('pdfs', JSON.stringify(obj))
        this.pdfs = obj
        break;
      } 
      case 'categorias':{
        localStorage.setItem('categorias', JSON.stringify(obj))
        this.categorias = obj
        break;
      }
    }    
  }

  getContent(type) {
    switch(type) {
      case 'pdfs': {
        this.pdfs = JSON.parse(localStorage.getItem('pdfs'))
        return this.pdfs
      }

      case 'categorias': {
        this.categorias = JSON.parse(localStorage.getItem('categorias'))
        return this.categorias
      }
    }
  }
}
