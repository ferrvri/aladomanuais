import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {finalize} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService implements OnInit {

  httpURL = 'http://aladoapp.com.br:21006/'
  // httpURL = "http://192.168.15.10:3000/"

  constructor(
    // private _http: HTTP
    private _http: HttpClient
  ) {
    // _http.setServerTrustMode("nocheck")
  }

  ngOnInit() {
    // this._http.setServerTrustMode('nocheck');
  }

  public get(url) {
    return new Promise( (resolve, reject) => {
      let h = this._http.get(this.httpURL + url, {}).pipe(finalize( () => h.unsubscribe())).subscribe( (response :any) => {
        resolve(response)
      },  err => {
        reject(err)
      })
    })
  }

  public post(url, body) {
    return new Promise( (resolve, reject) => {

      let h = this._http.post(this.httpURL + url, body, {}).pipe(finalize(() => h.unsubscribe())).subscribe( (response: any) => {
        resolve(response)
      }, err => {
        reject(err)
      })
    });
    
  }

  public _get(url) {
    return this._http.request('get', url, {
      responseType: "arraybuffer"
    })
  }
}
